Ext.ns('ERP.view.auth');
Ext.define('ERP.view.auth.AuthController', {
	extend: 'Ext.app.Controller',
	alias: 'controller.authtcontroller',

	requires: ['ERP.view.*'],
	/**
	 *
	 */
	constructor : function (config) {
		this.callParent(arguments);
	},

	/**
	 *
	 */
	doInit : function () {
		this.listen({
			component: {
				'[xtype=loginpanel] button#login': {
					click: this.doLogin
				}
			},
			global: {
				beforeviewportrender: this.processLoggedIn
			}
		});
	},

	processLoggedIn : function () {
		var me = this;

		// make remote request to check session
		Ext.Ajax.request({
			url: '/security/checklogin',
			method: 'POST',
			useDefaultXhrHeader : false,
			//withCredentials : true,
			cors : true,
			reader: {
				type: 'json',
				successProperty: 'Success',
				rootProperty: 'Data',
				messageProperty: 'message'
			},
			jsonData : {
				"token": Ext.util.Cookies.get("session")
			},
			success: function( response, options ) {
				// decode response
				var result = Ext.decode( response.responseText );
				// check if success flag is true
				if( result.success ) {
					// fire global event aftervalidateloggedin
					Ext.globalEvents.fireEvent( 'aftervalidateloggedin' );
					container.getMainPanel();
				} else {
					Ext.widget('loginpanel').show();
				}
			},
			failure: function( response, options ) {
				Ext.Msg.alert( 'Attention', 'Sorry, an error occurred during your request. Please try again.' );
			}
		});
	},

	doLogin : function( button, e, eOpts ) {
		var me = this;
		var win = button.up( 'panel' );
		var form = win.down( 'form' );
		var values = form.getValues();
		// make remote request to check session
		Ext.Ajax.request({
			url: '/security/login',
			method: 'POST',
			reader: {
				type: 'json',
				successProperty: 'Success',
				rootProperty: 'Data',
				messageProperty: 'message'
			},
			jsonData : {
				"userid": values.username,
				"pwd": values.password
			},
			success: function( response, options ) {
				var result = Ext.decode( response.responseText );
				// check if success flag is true
				if( result.success ) {
					Ext.globalEvents.fireEvent( 'aftervalidateloggedin' );
					// close window
					win.close();
					container.getMainPanel();
				}
				// couldn't login...show error
				else {
					Ext.Msg.alert( 'Attention', result.message );
				}
			},
			failure: function( response, options ) {
				Ext.Msg.alert( 'Attention', 'Sorry, an error occurred during your request. Please try again.' );
			}
		});
		//Ext.create({xtype : 'loginpanel'})
	}
});
