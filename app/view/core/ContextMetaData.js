Ext.ns('ERP.view.core');
/**
 * @class ERP.view.core.ContextMetaData
 * @extends ERP.view.core.PluginMetaData
 *
 * The Meta Data object containing the registration details
 * of a {@link ERP.view.core.Context}. An instance of this object
 * must be passed to {@link ERP.view.core.Container#registerContext}.
 */
Ext.define('ERP.view.core.ContextMetaData', {
    extend : 'ERP.view.core.PluginMetaData',
	/**
	 * @constructor
	 * @param {Object} config Configuration object
	 */
	constructor : function(config)
	{
		config = config || {};

		Ext.applyIf(config, {
			// By default Contexts cannot be disabled
			allowUserDisable : false
		});

		this.callParent(arguments);
	}
});
