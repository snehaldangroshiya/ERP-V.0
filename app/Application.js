Ext.ns('ERP');

Ext.apply(ERP, {
	/**
	 *
	 */
	isReady : false,
	/**
	 *
	 */
	name: 'ERP',
	/**
	 *
	 */
	controllers : 'ERP.view.auth.AuthController',
	/**
	 *
	 */
	readyEvent : new Ext.util.Event([]),

	/**
	 *
	 * @param fn
	 * @param scope
	 * @param options
	 */
	onReady : function(fn, scope, options)
	{
		this.readyEvent.addListener(fn, this, options);
		if (this.isReady) {
			this.fireReady();
		}
	},

	/**
	 *
	 */
	fireReady : function()
	{
		this.isReady = true;
		this.readyEvent.fire();
		this.readyEvent.clearListeners();
	},

	/**
	 *
	 */
	launch: function () {
		container = new ERP.view.core.Container();
		ERP.fireReady();

		// Initialize context - check if there is one selected in settings
		this.initContext();
		Ext.globalEvents.fireEvent( 'beforeviewportrender' );
	},

	/**
	 * init UI by lazily constructing the main panel (implicit in container.getMainPanel) and
	 * setting the default context to visible. Note that during onHierarchyLoad we will also
	 * open the default folder for that specific context to ensure the user can see all from
	 * the folder.
	 * @private
	 */
	initContext : function()
	{
		//var defaultContext = container.getSettingsModel().get('zarafa/v1/main/default_context');
		var plugin = container.getContextByName('mail');
		if (!plugin) {
			// If the defaultContext has an invalid name,
			// we will default to the 'today' context
			plugin = container.getContextByName('today');
		}
		container.switchContext(plugin);
	}
});

Ext.application(ERP);