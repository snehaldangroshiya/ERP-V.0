package core

import (
	"labix.org/v2/mgo/bson"
)

type Props struct {
	Id                              bson.ObjectId       `bson:"_id,omitempty" json:"-"`
	PR_ENTRYID                      string              `json:"entryid,omitempty" bson:"entryid,omitempty"`
	PR_PARENT_ENTRYID               string              `json:"parent_entryid,omitempty" bson:"parent_entryid,omitempty"`
	PR_STORE_ENTRYID                string              `json:"store_entryid,omitempty" bson:"store_entryid,omitempty"`
	PR_STORE_RECORD_KEY             string              `json:"store_record_key,omitempty" bson:"store_record_key,omitempty"`
	PR_RECORD_KEY                   string              `json:"record_key,omitempty" bson:"record_key,omitempty"`
	PR_DISPLAY_NAME                 string              `json:"display_name,omitempty" bson:"display_name,omitempty"`
	PR_DEFAULT_STORE                bool                `json:"default_store,omitempty" bson:"default_store,omitempty"`
	PR_STORE_SUPPORT_MASK           int                 `json:"store_support_mask,omitempty" bson:"store_support_mask,omitempty"`
	PR_STORE_STATE                  int                 `json:"store_state,omitempty" bson:"store_state,omitempty"`
	PR_IPM_SUBTREE_SEARCH_KEY       string              `json:"ipm_subtree_search_key,omitempty" bson:"ipm_subtree_search_key ,omitempty"`
	PR_IPM_OUTBOX_SEARCH_KEY        string              `json:"ipm_outbox_search_key,omitempty" bson:"ipm_outbox_search_key,omitempty"`
	PR_IPM_WASTEBASKET_SEARCH_KEY   string              `json:"ipm_wastebasket_search_key,omitempty" bson:"ipm_wastebasket_search_key,omitempty"`
	PR_IPM_SENTMAIL_SEARCH_KEY      string              `json:"ipm_sentmail_search_key,omitempty" bson:"ipm_sentmail_search_key,omitempty"`
	PR_MDB_PROVIDER                 string              `json:"mdb_provider,omitempty" bson:"mdb_provider,omitempty"`
	PR_RECEIVE_FOLDER_SETTINGS      string              `json:"receive_folder_settings,omitempty" bson:"receive_folder_settings,omitempty"`
	PR_VALID_FOLDER_MASK            string              `json:"valid_folder_mask ,omitempty" bson:"valid_folder_mask,omitempty"`
	PR_IPM_SUBTREE_ENTRYID          string              `json:"ipm_subtree_entryid,omitempty" bson:"ipm_subtree_entryid,omitempty"`
	PR_IPM_OUTBOX_ENTRYID           string              `json:"ipm_outbox_entryid,omitempty" bson:"ipm_outbox_entryid,omitempty"`
	PR_IPM_WASTEBASKET_ENTRYID      string              `json:"ipm_wastebasket_entryid,omitempty" bson:"ipm_wastebasket_entryid,omitempty"`
	PR_IPM_SENTMAIL_ENTRYID         string              `json:"ipm_sentmail_entryid,omitempty" bson:"ipm_sentmail_entryid,omitempty"`
	PR_VIEWS_ENTRYID                string              `json:"views_entryid,omitempty" bson:"views_entryid,omitempty"`
	PR_COMMON_VIEWS_ENTRYID         string              `json:"common_views_entryid,omitempty" bson:"common_views_entryid,omitempty"`
	PR_FINDER_ENTRYID               string              `json:"finder_entryid,omitempty" bson:"finder_entryid,omitempty"`
	PR_IPM_FAVORITES_ENTRYID        string              `json:"ipm_favorites_entryid,omitempty" bson:"ipm_favorites_entryid,omitempty"`
	PR_IPM_PUBLIC_FOLDERS_ENTRYID   string              `json:"ipm_public_folders_entryid,omitempty" bson:"ipm_public_folders_entryid,omitempty"`
	PR_IPM_APPOINTMENT_ENTRYID      string              `json:"ipm_appointment_entryid,omitempty" bson:"ipm_appointment_entryid,omitempty"`
	PR_IPM_CONTACT_ENTRYID          string              `json:"ipm_contact_entryid,omitempty" bson:"ipm_contact_entryid,omitempty"`
	PR_IPM_JOURNAL_ENTRYID          string              `json:"ipm_journal_entryid,omitempty" bson:"ipm_journal_entryid,omitempty"`
	PR_IPM_NOTE_ENTRYID             string              `json:"ipm_note_entryid,omitempty" bson:"ipm_note_entryid,omitempty"`
	PR_IPM_TASK_ENTRYID             string              `json:"ipm_task_entryid,omitempty" bson:"ipm_task_entryid,omitempty"`
	PR_IPM_DRAFTS_ENTRYID           string              `json:"ipm_drafts_entryid,omitempty" bson:"ipm_drafts_entryid,omitempty"`
	PR_EC_ERP_SETTINGS_JSON         string              `json:"ec_erp_settings_json,omitempty" bson:"ec_erp_settings_json,omitempty"`
	PR_OBJECT_TYPE                  int                 `json:"object_type,omitempty" bson:"object_type,omitempty"`
	PR_FOLDER_TYPE                  string              `json:"folder_type,omitempty" bson:"folder_type,omitempty"`
	PR_MAPPING_SIGNATURE            string              `json:"mapping_signature,omitempty" bson:"mapping_signature,omitempty"`
	PR_SUBFOLDERS                   bool                `json:"subfolders,omitempty" bson:"subfolders,omitempty"`
	PR_ACCESS                       int                 `json:"access,omitempty" bson:"access,omitempty"`
	PR_SUBJECT                      string              `json:"subject,omitempty" bson:"subject,omitempty"`
	PR_ICON_INDEX                   int                 `json:"icon_index,omitempty" bson:"icon_index,omitempty"`
	PR_MESSAGE_CLASS                string              `json:"message_class,omitempty" bson:"message_class,omitempty"`
	PR_MESSAGE_FLAGS                string              `json:"message_flags,omitempty" bson:"message_flags,omitempty"`
	PR_MESSAGE_SIZE                 int                 `json:"message_size,omitempty" bson:"message_size,omitempty"`
	PR_MIDDLE_NAME                  string              `json:"middle_name,omitempty" bson:"middle_name,omitempty"`
	PR_SURNAME                      string              `json:"surname,omitempty" bson:"surname,omitempty"`
	PR_HOME_TELEPHONE_NUMBER        int                 `json:"home_telephone_number,omitempty" bson:"home_telephone_number,omitempty"`
	PR_CELLULAR_TELEPHONE_NUMBER    int                 `json:"cellular_telephone_number,omitempty" bson:"cellular_telephone_number,omitempty"`
	PR_BUSINESS_TELEPHONE_NUMBER    int                 `json:"business_telephone_number,omitempty" bson:"business_telephone_number,omitempty"`
	PR_BUSINESS_FAX_NUMBER          int                 `json:"business_fax_number,omitempty" bson:"business_fax_number,omitempty"`
	PR_COMPANY_NAME                 string              `json:"company_name,omitempty" bson:"company_name,omitempty"`
	PR_DEPARTMENT_NAME              string              `json:"department_name,omitempty" bson:"department_name,omitempty"`
	PR_OFFICE_LOCATION              string              `json:"office_location,omitempty" bson:"office_location,omitempty"`
	PR_PROFESSION                   string              `json:"profession,omitempty" bson:"profession,omitempty"`
	PR_MANAGER_NAME                 string              `json:"manager_name,omitempty" bson:"manager_name,omitempty"`
	PR_ASSISTANT                    string              `json:"assistant,omitempty" bson:"assistant,omitempty"`
	PR_NICKNAME                     string              `json:"nickname,omitempty" bson:"nickname,omitempty"`
	PR_DISPLAY_NAME_PREFIX          string              `json:"display_name_prefix,omitempty" bson:"display_name_prefix,omitempty"`
	PR_SPOUSE_NAME                  string              `json:"spouse_name,omitempty" bson:"spouse_name,omitempty"`
	PR_GENERATION                   string              `json:"generation,omitempty" bson:"generation,omitempty"`
	PR_BIRTHDAY                     string              `json:"birthday,omitempty" bson:"birthday,omitempty"`
	PR_WEDDING_ANNIVERSARY          string              `json:"wedding_anniversary,omitempty" bson:"wedding_anniversary,omitempty"`
	PR_SENSITIVITY                  string              `json:"sensitivity,omitempty" bson:"sensitivity,omitempty"`
	PR_HASATTACH                    bool                `json:"hasattach,omitempty" bson:"hasattach,omitempty"`
	PR_COUNTRY                      string              `json:"country,omitempty" bson:"country,omitempty"`
	PR_LOCALITY                     string              `json:"locality,omitempty" bson:"locality,omitempty" `
	PR_POSTAL_ADDRESS               string              `json:"postal_address,omitempty" bson:"postal_address,omitempty"`
	PR_POSTAL_CODE                  int                 `json:"postal_code,omitempty" bson:"postal_code,omitempty"`
	PR_STATE_OR_PROVINCE            string              `json:"state_or_province,omitempty" bson:"state_or_province,omitempty"`
	PR_STREET_ADDRESS               string              `json:"street_address,omitempty" bson:"street_address,omitempty"`
	PR_SESSION_ID                   string              `json:"session_id,omitempty" bson:"session_id,omitempty"`
}

func GetPropsStructs() *Props {
	return &Props{}
}