package core

import (
	"net/http"
	"encoding/json"
)
type Bus struct {}

func GetBus() *Bus {
	return &Bus{}
}
/*
	modulename : "maillistitemmodule",
	entryid : "",
	parentEntryid : "",
	storeEntryid : "",
	action : "list","save","open","delete","search"
	props : [{
		"display_name" : "snehal"
		"address"
	}]
	}
*/
type Request struct {
	ModuleName string `json:"module_name"`
	Entryid string `json:"entryid"`
	ParentEntryid string `json:"parent_entryid"`
	storeEntryid string `json:"store_entryid"`
	Action string `json:"action"`
	Props
}

type ModuleInfo struct {
	ModuleName string `json:"module_name"`
}
/**
{
	"module_name" : "maillistitemmodule",
	"entryid" : "",
	"parent_entryid" : "",
	"store_entryid" : "",
	"action" : "list"
}
 */
func (bus *Bus) GetModuleName(w http.ResponseWriter, r *http.Request) string {
	var moduleName ModuleInfo
	decode := json.NewDecoder(r.Body)
	decode.Decode(&moduleName)
	return moduleName.ModuleName
}
