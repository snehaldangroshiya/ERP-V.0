package main

import (
	"log"
	"net/http"

	"github.com/ERP/authentication"
	"github.com/gorilla/mux"
	"github.com/ERP/provider"
)

func init() {
	syncDB()
}

func main() {
	router := mux.NewRouter()
	uc := authentication.NewUserController()
	router.HandleFunc("/security/login", uc.Login)
	router.HandleFunc("/security/checklogin", uc.SessionCheck)
	//router.HandleFunc("/security/logout", authentication.logOut)
	router.PathPrefix("/").Handler(http.FileServer(http.Dir("../")))
	log.Fatal(http.ListenAndServe(":8000", router))
}

//	Function will sync the database to get updates stores table.
func syncDB() {
	provider.InitProvider()
}