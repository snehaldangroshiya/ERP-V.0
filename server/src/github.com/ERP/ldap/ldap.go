package ldap

import (
	"log"
	"fmt"
	"gopkg.in/ldap.v2"
)

func Authentication(username string, password string) (bool,string){
	bindusername := "cn=admin,dc=zarafa,dc=local"
	bindpassword := "a"

	l, err := ldap.Dial("tcp", fmt.Sprintf("%s:%d", "zarafa.local", 389))
	if err != nil {
	    log.Fatal(err)
	}
	defer l.Close()

	// First bind with a read only user
	err = l.Bind(bindusername, bindpassword)
	if err != nil {
		return false, ""
	}

	// Search for the given username
	searchRequest := ldap.NewSearchRequest(
	    "dc=zarafa,dc=local",
	    ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
	    fmt.Sprintf("(&(uid=%s))", username),
	    []string{"dn", "cn"},
	    nil,
	)

	sr, err := l.Search(searchRequest)
	if err != nil {
		return false, ""
	}

	if len(sr.Entries) != 1 {
		return false, ""
	}
	
	userdn := sr.Entries[0].DN

	// Bind as the user to verify their password
	err = l.Bind(userdn, password)
	if err != nil {
	    return false, ""
	}
	return true, sr.Entries[0].GetAttributeValue("cn")
}

// Function retrieve all users from LDAP server.
func GetAllUsers() (users []interface{}, error error) {
	l, err := ldap.Dial("tcp", fmt.Sprintf("%s:%d", "zarafa.local", 389))
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()

	searchRequest := ldap.NewSearchRequest(
		"dc=zarafa,dc=local", // The base dn to search
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		"(&(objectClass=zarafa-user))", // The filter to apply
		[]string{"dn", "cn", "sn", "mail","userpassword"},// A list attributes to retrieve
		nil,
	)

	sr, err := l.Search(searchRequest)
	if err != nil {
		error = err
		log.Fatal(err)
	}

	for _ , entry := range sr.Entries {
		users = append(users, entry.GetAttributeValue("cn"))
	}
	return
}
