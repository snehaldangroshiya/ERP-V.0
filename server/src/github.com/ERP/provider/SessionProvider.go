package provider

import (
	"fmt"
	"net/http"
	"github.com/satori/go.uuid"
	"sync"
	"log"
	"github.com/ERP/mongo"
	"github.com/ERP/core"
)

var sessionStore map[string]Client
var storageMutex sync.RWMutex

type Client struct {
	LoggedIn bool `json:"success"`
}

func init() {
	sessionStore = make(map[string]Client)
}

func InitSession(w http.ResponseWriter, r *http.Request, cn string) {
	var client Client

	cookie, err := r.Cookie("session")
	if err != nil {
		if err != http.ErrNoCookie {
			fmt.Fprint(w, err)
			return
		} else {
			err = nil
		}
	}

	var present bool

	if cookie != nil {
		storageMutex.RLock()
		client, present = sessionStore[cookie.Value]
		storageMutex.RUnlock()
	} else {
		present = false
	}

	if present == false {
		cookie = &http.Cookie{
			Name:  "session",
			Value: uuid.NewV4().String(),
			Path:  "/",
		}
		client = Client{true}
		storageMutex.Lock()
		sessionStore[cookie.Value] = client
		storageMutex.Unlock()
	}

	// Make an entry in SESSIONS table.
	props:=core.GetPropsStructs();
	props.PR_DISPLAY_NAME = cn
	props.PR_SESSION_ID = cookie.Value
	if error:=mongo.CreateDocument(core.SESSIONS_TABLE, props); error != nil {
		log.Println("Error: Record is not properly inserted in",core.SESSIONS_TABLE)
	}

	http.SetCookie(w, cookie)
}

//
func IsSessionActive(r *http.Request) bool {
	value, err :=GetSessionId(r)
	if err != nil {
		return false
	} else {
		return sessionStore[value.String()].LoggedIn
	}
}

func GetSessionId(r *http.Request) (uid uuid.UUID,er error) {
	cookie, er:=r.Cookie("session")
	if er != nil {
		return uuid.Nil,er
	}
	uid,err:= uuid.FromString(cookie.Value)
	return uid, err

}