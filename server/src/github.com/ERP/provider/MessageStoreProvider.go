package provider

import (
	"github.com/ERP/ldap"
	"log"
	"github.com/ERP/core"
	"github.com/ERP/mongo"
	"labix.org/v2/mgo/bson"
	"strconv"
)

//
func InitMessageBoxProvider() {
	var users []interface{}
	var err error
	if users ,err  =  ldap.GetAllUsers(); err != nil{
		log.Println("ERROR : while retriving user info from LDAP server")
	}

	for _, v := range users {
		var row []bson.M
		var err error
		if row, err = mongo.GetUserByDisplayName(core.STORES_TABLE,v.(string), 0,-1); err != nil {
			log.Println("ERROR: while retriving record from MongoDB")
		} else if row == nil {
			CreateMessageStore(v.(string))
		}
	}
}

//
func CreateMessageStore(userName string)  {
	props := core.GetPropsStructs()
	props.PR_DISPLAY_NAME = userName
	props.Id = bson.NewObjectId()
	var err error
	if err = mongo.CreateDocument(core.STORES_TABLE, props); err != nil {
		log.Println("Could not save the entry to MongoDB:", err)
	}

	props.PR_STORE_ENTRYID = GetProvider().GenerateStoreEntryId(props.Id)
	props.PR_ENTRYID = props.PR_STORE_ENTRYID
	props.PR_DISPLAY_NAME = "Inbox - "+ userName
	props.PR_MDB_PROVIDER = props.Id.Hex()//storeObjId.Hex()
	props.Id = bson.NewObjectId()
	props.PR_STORE_RECORD_KEY = props.Id.Hex()
	props.PR_RECORD_KEY = props.PR_STORE_RECORD_KEY
	props.PR_OBJECT_TYPE = core.MAPI_STORE
	props.PR_MAPPING_SIGNATURE = props.PR_STORE_RECORD_KEY
	err = mongo.CreateDocument(core.OBJECTS_TABLE, props)

	if err != nil {
		log.Println("ERROR : Could not save the entry to MongoDB:", err)
	}

	if err = CreateRootContainer(props); err != nil {
		log.Println("ERROR: Root container is not created properly")
	}
}

func CreateRootContainer(msgStore *core.Props) error {
	props := core.GetPropsStructs()
	props.PR_FOLDER_TYPE = strconv.Itoa(core.FOLDER_ROOT)
	props.PR_SUBFOLDERS = true
	entryid , err := CreateFolder(props, msgStore, msgStore)
	if err != nil {
		log.Println("Error : IPM Subtree is not created properly", err)
	}

	props.PR_PARENT_ENTRYID = entryid
	if err := mongo.UpdateDocument(core.OBJECTS_TABLE, props.Id, props); err != nil {
		log.Println("ERROR : Could not save the parent entry of root container to MongoDB:", err)
	}
	CreateDefaultFolders(props)
	return err
}

func DefaultMessageStore(user string) (*core.Props) {
	props := core.GetPropsStructs();

	return props
}

func getRootContainer() *core.Props {
	props := core.GetPropsStructs();

	return props
}

//
func CreateDefaultFolders(parent *core.Props) {
	if err := CreateIPMSubTree(parent); err != nil{
		log.Println("Error : IMP_SUBTREE is not created properly", err)
	}
}

func CreateFolder(props *core.Props, parent *core.Props, store *core.Props) (string, error) {
	props.Id = bson.NewObjectId()
	props.PR_OBJECT_TYPE = core.MAPI_FOLDER
	props.PR_STORE_ENTRYID = parent.PR_STORE_ENTRYID
	props.PR_MDB_PROVIDER = parent.PR_MDB_PROVIDER
	props.PR_MAPPING_SIGNATURE = parent.PR_MAPPING_SIGNATURE
	props.PR_STORE_RECORD_KEY =  parent.PR_MAPPING_SIGNATURE

	// generate Entryid for default folders
	props.PR_ENTRYID = GetProvider().GenerateEntryId(core.OBJECTS_TABLE, store, props)
	props.PR_RECORD_KEY = props.PR_ENTRYID

	// Create document for the default folder in Object table
	err := mongo.CreateDocument(core.OBJECTS_TABLE, props)
	if err != nil {
		log.Println("ERROR : Could not save the entry to MongoDB:", err)
	}
	return props.PR_ENTRYID, err
}

func GetMsgStoreObj(folderProps *core.Props) *core.Props{
	props := core.GetPropsStructs();
	var query interface{} = bson.M{"store_entryid": folderProps.PR_STORE_ENTRYID, "object_type": core.MAPI_STORE}
	if msgStoreProps, e:= mongo.Search(core.OBJECTS_TABLE, query, 0, 1); e == nil {
		for _, v := range msgStoreProps {
			b, _ :=bson.Marshal(v)
			bson.Unmarshal(b, props)
		}
	}
	return props
}

func CreateIPMSubTree(parent *core.Props) error {
	props := core.GetPropsStructs()
	props.PR_PARENT_ENTRYID = parent.PR_ENTRYID
	props.PR_FOLDER_TYPE = strconv.Itoa(core.FOLDER_GENERIC)
	props.PR_DISPLAY_NAME = "IPM_SUBTREE"

	msgStore := GetMsgStoreObj(parent)
	entryid , err := CreateFolder(props, parent, msgStore)
	if err != nil {
		log.Println("Error : IPM Subtree is not created properly", err)
	}

	msgStore.PR_IPM_SUBTREE_ENTRYID = entryid
	// Update message store document in object store.
	if err := mongo.UpdateDocument(core.OBJECTS_TABLE, msgStore.Id, msgStore); err != nil {
		log.Println("ERROR : Could not save the entry to MongoDB:", err)
	}

	if err := CreateDeletedItemsFolder(props); err != nil {
		log.Println("Error : Deleted items folder is not created properly", err)
	}
	return err
}

func CreateDeletedItemsFolder(parent *core.Props) error {
	props := core.GetPropsStructs()
	props.PR_PARENT_ENTRYID = parent.PR_ENTRYID
	props.PR_FOLDER_TYPE = strconv.Itoa(core.FOLDER_GENERIC)
	props.PR_DISPLAY_NAME = "Deleted Items"

	msgStore := GetMsgStoreObj(parent)
	entryid , err := CreateFolder(props, parent, msgStore)
	if err != nil {
		log.Println("Error : IPM Subtree is not created properly", err)
	}

	msgStore.PR_IPM_WASTEBASKET_ENTRYID = entryid
	// Update message store document in object store.
	if err := mongo.UpdateDocument(core.OBJECTS_TABLE, msgStore.Id, msgStore); err != nil {
		log.Println("ERROR : Could not save the entry to MongoDB:", err)
	}

	return err
}
