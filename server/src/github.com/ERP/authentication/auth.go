package authentication
/**
	TODO Task list
	1. Create server model which is responsible for following task
		login
		logout
		Session Checking
		Session Create/Delete
 */

import (
	"encoding/json"
	"net/http"
	"github.com/ERP/ldap"
	"github.com/ERP/provider"
)

type ErrorMsg struct {
	Message string `json:"message"`
}
type User struct {
	UserId string `json:"userid"`
	Pwd    string `json:"pwd,omitempty"`
	Token  string `json:"token,omitempty"`
	Auth   bool   `json:"auths"`
}

type response struct {
	Success     bool `json:"success"`
}

type (
	UserController struct{}
)

func NewUserController() *UserController {
	return &UserController{}
}

func (uc UserController) Login(w http.ResponseWriter, r *http.Request) {
	var ur User
	decode := json.NewDecoder(r.Body)
	decode.Decode(&ur)
	auth,cn := ldap.Authentication(ur.UserId, ur.Pwd)
	if auth == true {
		var re response
		re.Success = true
		provider.InitSession(w, r, cn)
		json.NewEncoder(w).Encode(re)
	} else {
		var re ErrorMsg
		re.Message = "Please check your credential"
		json.NewEncoder(w).Encode(re)
	}
}

/**
 * Function used to check the user is currently logged in or not
 */
func (uc UserController) IsLogon(w http.ResponseWriter, r *http.Request) bool {
	return provider.IsSessionActive(r)
}

/**
 * Check that session is exist or not
 */
func (uc UserController) SessionCheck(w http.ResponseWriter, r *http.Request) {
	re := &response{}
	re.Success = uc.IsLogon(w, r)
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(re)
}
