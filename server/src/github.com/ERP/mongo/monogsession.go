package mongo

import (
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
)

var(
	mgoSession *mgo.Session
	databaseName = "ERP"
)

func getSession () *mgo.Session {
	if mgoSession == nil {
		var err error
		mgoSession, err = mgo.Dial("localhost")
		if err != nil {
			panic(err) // no, not really
		}
	}
	return mgoSession.Clone()
}

func withCollection(collection string, s func(*mgo.Collection) error) error {
	session := getSession()
	defer session.Close()
	c := session.DB(databaseName).C(collection)
	return s(c)
}

func Search (collectionName string, q interface{}, skip int, limit int) (searchResults []bson.M, e error) {
	query := func(c *mgo.Collection) error {
		fn := c.Find(q).Skip(skip).Limit(limit).All(&searchResults)
		if limit < 0 {
			fn = c.Find(q).Skip(skip).All(&searchResults)
		}
		return fn
	}

	search := func() error {
		return withCollection(collectionName, query)
	}

	err := search()
	if err != nil {
		e = err
	}
	return
}

//
//
//
func GetUserByDisplayName (collectionName string, name string, skip int, limit int) (searchResults []bson.M, err error) {
	searchResults, err = Search(collectionName, bson.M{"display_name": name}, skip, limit)
	return
}

//
//
//
func GetIDByDisplayName (collectionName string, name string, skip int, limit int) (_id bson.ObjectId, err error) {
	var searchResults []bson.M
	searchResults, err = Search(collectionName, bson.M{"display_name": name}, skip, limit)
	Obj , _ := searchResults[0]["_id"].(bson.ObjectId)
	_id = Obj
	return
}

//
//
//
func CreateDocument(collectionName string, props interface{}) error {
	query := func(c *mgo.Collection) error {
		fn := c.Insert(props)
		return fn
	}
	insert := func() error {
		return withCollection(collectionName, query)
	}
	return insert()
}

// update given collection document with given properties
func UpdateDocument(collectionName string, id bson.ObjectId, props interface{}) error {
	query := func(c *mgo.Collection) error {
		fn := c.UpdateId(id, bson.M{"$set":props})
		return fn
	}
	update := func() error {
		return withCollection(collectionName, query)
	}
	return update()
}

//
//
//
func GetGlobeCounter(collectionName string) (int,error) {
	session := getSession();
	defer session.Close()
	return session.DB(databaseName).C(collectionName).Count()
}
