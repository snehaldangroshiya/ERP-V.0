Ext.namespace('ERP.view.core');

/**
 * @class ERP.core.ui.MainContentTabPanel
 * @extends Ext.tab.Panel
 *
 * This subclass is used in the main content area, and contains the ContextContainer and dialogs that the user opens
 * Initialized in MainViewport.CreateContentContainer
 */
Ext.define('ERP.view.core.MainContentTabPanel', {

	extend : 'Ext.container.Container'
});
