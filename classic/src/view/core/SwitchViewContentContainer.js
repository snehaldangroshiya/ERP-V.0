Ext.namespace('ERP.view.core');

/**
 * @class Party.core.ui.SwitchViewContentContainer
 * @extends Ext.Container
 * @xtype party.switchviewcontentcontainer
 *
 * This class represents an {@link Ext.Panel panel} which contains multiple views which
 * can be enabled/disabled at any time by the user. Using lazy loading each view will only
 * be allocated when the user switches to the given view for the first time.
 */

Ext.define('ERP.view.core.SwitchViewContentContainer', {

    extend : 'Ext.container.Container',

    alias : 'widget.switchviewcontentcontainer',
    /**
     * @cfg {Object[]} lazyItems List of {@link Ext.Containers containers} which
     * act as view for this {@link Ext.Container container}. This array consists of configuration
     * objects which will be allocated when the user switches to the given view for the
     * first time.
     * All items added to this list must always contain the 'id' property,
     * used in switchView. Without this property it is not possible to switch
     * to this view.
     */
    lazyItems : undefined,

    /**
     * @cfg {Boolean} autoClean If true this container will automatically remove
     * and delete the previously selected {@link Ext.Container container} when switching
     * to a new active {@link Ext.Container container}.
     */
    autoClean : true,

    /**
     * @constructor
     * @param {Object} config Configuration object
     */
    constructor : function(config)
    {
        config = config || {};

        // Always ensure that at least 1 item is non-lazy
        if (Ext.isEmpty(config.items) && !Ext.isEmpty(config.lazyItems)) {
            config.items = [ config.lazyItems[0] ];
        }
        // Ensure that the non-lazy item is marked as active
        if (Ext.isEmpty(config.activeItem)) {
            config.activeItem = config.items[0].id;
        }

        Ext.applyIf(config, {
            autoDestroy: true
        });

        this.callParent(arguments);
    }
});
