Ext.ns('ERP.view.core');
Ext.define('ERP.view.core.MainViewPort', {
	/**
	 *
	 */
	extend: 'Ext.container.Viewport',

	/**
	 *
	 */
	xtype: 'mainviewport',

	/**
	 *
	 */
	alias: 'widget.mainviewport',

	/**
	 *
	 */
	requires : [
		'ERP.view.mail.MailContext',
		'ERP.view.crm.CRMContext',
		'ERP.view.core.*',
		'Ext.ux.layout.ResponsiveColumn'
	],

	/**
	 * @constructor
	 * @param {Object} config Configuration object
	 */
	constructor : function(config)
	{
		config = config || {};

		config = Ext.applyIf(config, {
			layout : 'fit',
			items : [{
				xtype: 'container',
				layout : 'border',
				border : false,
				items : [
					this.createContentContainer(),
					this.createNavigationPanel(),
					this.createNavigationToolbar()
				]
			}]
		});
		this.callParent(arguments);
	},

	/**
	 *
	 */
	createNavigationToolbar : function()
	{
		return {
			xtype: 'toolbar',
			region : 'north',
			cls: 'sencha-dash-dash-headerbar toolbar-btn-shadow shadow-panel',
			height: 64,
			itemId: 'headerBar',
			items: [{
					margin: '0 0 0 8',
					cls: 'delete-focus-bg',
					iconCls:'erp erp-left',
					id: 'main-navigation-btn',
					handler: 'onToggleMicro'
				},
				{
					xtype: 'tbspacer',
					flex: 1
				},
				{
					cls: 'delete-focus-bg',
					iconCls:'x-fa fa-search',
					href: '#search',
					hrefTarget: '_self',
					tooltip: 'See latest search'
				},
				{
					cls: 'delete-focus-bg',
					iconCls:'x-fa fa-envelope',
					href: '#email',
					hrefTarget: '_self',
					tooltip: 'Check your email'
				},
				{
					cls: 'delete-focus-bg',
					iconCls:'x-fa fa-bell'
				},
				{
					cls: 'delete-focus-bg',
					iconCls:'x-fa fa-th-large',
					href: '#profile',
					hrefTarget: '_self',
					tooltip: 'See your profile'
				},
				{
					cls: 'delete-focus-bg',
					iconCls:'x-fa fa-th-large',
					//href: 'security/login',
					handler : function () {
						console.log("ths")
					},
					hrefTarget: '_self',
					tooltip: 'See your profile'
				},
				{
					xtype: 'tbtext',
					text: 'Goff Smith',
					cls: 'top-user-name'
				}
			]
		}
	},

	/**
	 * Create the {@link Zarafa.core.ui.NavigationPanel NavigationPanel} for
	 * the west region of the client in which the treePanel can be shown.
	 * @return {Zarafa.core.ui.NavigationPanel} NavigationPanel.
	 * @private
	 */
	createNavigationPanel : function()
	{
		this.navigationPanel = new ERP.view.core.NavigationPanel({
			region : 'west',
			cls: 'shadow-panel'

		});
		return this.navigationPanel;
	},

	/**
	 *
	 * @returns {ERP.view.core.MainContentTabPanel|*}
	 */
	createContentContainer : function()
	{
		var cc = new ERP.view.core.ContextContainer({
			name : 'main.content',
			id: 'erp-mainpanel-content'
		});

		/**
		 * Move this line to ERP.view.mail.MainContentTabPanel constructor.
		 */
		var lazyItems = container.populateInsertionPoint('main.content.tabpanel', this);

		this.contentPanel = new ERP.view.core.MainContentTabPanel({
			id: 'zarafa-mainpanel',
			activeTab : 0,
			scrollable : true,
			region : 'center',
			enableTabScroll : true,
			layoutOnTabChange : true,
			items : [ cc ].concat(lazyItems),
			cls : 'zarafa-body-tabbar'
		});

		return this.contentPanel;
	}
});
