Ext.ns('ERP.view.core');
Ext.define('ERP.view.core.ContextNavigationPanel', {
	/**
	 *
	 */
	extend: 'Ext.panel.Panel',
	/**
	 *
	 */
	alias : 'widget.contextnavigation',
	/**
	 * For this Context this panel will be visible in the {@link Zarafa.core.ui.NavigationPanel NavigationPanel}.
	 * @cfg {Zarafa.core.Context} Related Context
	 */
	context: null,

	/**
	 * @constructor
	 * @param {Object} config configuration object
	 */
	constructor : function (config) {
		config = config || {};

		// Config options for component itself.
		Ext.applyIf(config, {
			border : false,
			layout: 'fit',
			defaults : {
				border : false,
				autoScroll : false,
				defaults : { cls : 'zarafa-context-navigation-item-body' }
			}
		});

		this.callParent(arguments);
	},

	/**
	 * @return {Zarafa.core.Context}
	 */
	getContext : function() {
		return this.context || false;
	}
});