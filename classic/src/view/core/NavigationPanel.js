Ext.namespace('ERP.view.core');

Ext.define('ERP.view.core.NavigationPanel', {
	/**
	 *
	 */
	extend : 'ERP.view.core.MainViewSidebar',

	/**
	 *
	 */
	centerPanel: null,

	/**
	 * @constructor
	 * @param {Object} config configuration object
	 */
	constructor : function (config)
	{
		config = config || {};

		// Collect components for 'north', 'center' and 'south' from registered plugins
		var northComponents = container.populateInsertionPoint('navigation.north', this);
		var centerComponents = container.populateInsertionPoint('navigation.center', this);
		var southComponents = container.populateInsertionPoint('navigation.south', this);

		for (var i = 0, len = northComponents.length; i < len; i++){
			northComponents[i] = Ext.create(northComponents[i]);
		}

		for (var i = 0, len = centerComponents.length; i < len; i++){
			centerComponents[i] = Ext.create(centerComponents[i]);
		}

		for (var i = 0, len = southComponents.length; i < len; i++){
			southComponents[i] = Ext.create(southComponents[i]);
		}
		var items = [];
		items.push.apply(items, northComponents);

		items.push({
			xtype: 'container',
			ref: 'centerPanel',
			flex: 1,
			layout: {
				type : 'card',
				deferredRender : true
			},
			activeItem: 0,
			items: centerComponents
		});

		items.push.apply(items, southComponents);

		Ext.applyIf(config, {
			border : false,
			layout : {
				type: 'vbox',
				align: 'stretch'
			},
			id: 'zarafa-navigationpanel',
			cls: 'zarafa-navigation zarafa-panel zarafa-context-mainpanel',

			north : northComponents,
			center : centerComponents,
			south : southComponents,

			items : items
		});

		this.callParent(arguments);
	}
});