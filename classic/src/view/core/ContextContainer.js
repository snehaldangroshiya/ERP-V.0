Ext.namespace('ERP.view.core');
/**
 * @class ERP.view.core.ContextContainer
 * @extends ERP.view.core.SwitchViewContentContainer
 *
 * The ContextContainer is a light-weight ExtJS container that holds several child
 * components in a card layout, and is populated using an insertion point.
 * Each child item belongs to a ContentContext, and corresponding children are
 * made visible when contexts are switched. Two examples of ContextContainer objects
 * in the main layout are the top tool bar and the main content panel. Each available
 * content context (mail, tasks, etc) registers exactly one component with each named
 * context container (main.toolbar, main.content). When a folder is selected via
 * the hierarchy panel on the left and a context switch is needed (ie, when a mail
 * folder is selected the mail toolbar and mail content panel need to be shown) the
 * context containers will show the child items that correspond to the newly selected
 * context.
 */
Ext.define('ERP.view.core.ContextContainer', {

	extend : 'ERP.view.core.SwitchViewContentContainer',

	/**
	 * @cfg {String} name The name of the context container.
	 */
	name : undefined,

	/**
	 * @cfg {String} title The title of this component. Generally used only by owner container
	 */
	title : undefined,

	/**
	 * @constructor
	 * @param config configuration object
	 */
	constructor : function(config)
	{
		if (config.name)
			this.name = config.name;

		// Gather child components through an insertion point.
		var items = [];
		var lazyItems = container.populateInsertionPoint(this.name, config.scope);

		// Go over all plugins which have inserted components, each item needs to
		// be assigned with a unique ID which is made up out of this container's name
		// and the name of the plugins that inserted them. We use this for switching
		// between them.
		// At the same time we will be looking for components that were added by the
		// currently active context, as those will be set as the default component.
		var currentContext = container.getCurrentContext();
		Ext.each(lazyItems, function(item) {
			var pluginName = item.context.getName();

			// Check if this is the default component
			if (currentContext && pluginName === currentContext.getName()) {
				items.push(item);
			}
		}, this);

		// Standard configuration.
		Ext.applyIf(config, {
			autoDestroy : true,
			layout : 'card',
			deferredRender : 'false',
			hideMode : 'offsets',
			size : 'auto',
			border : false,
			lazyItems : lazyItems,
			items : items,
			activeItem : 0,
			forceLayout : true
		});

		this.callParent(arguments);
	}
});