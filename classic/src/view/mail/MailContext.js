Ext.ns('ERP.view.mail');
/**
 *
 */
Ext.define('ERP.view.mail.MailContext', {
	/**
	 *
	 */
	extend : 'ERP.view.core.Context',
	/**
	 *
	 */
	requires :[
		'ERP.view.mail.MailContextController',
		'ERP.view.mail.MailContextViewModel',
		'ERP.view.mail.*'
	],

	/**
	 *
	 */
	controller: 'mailcontextcontroller',

	/**
	 *
	 */
	viewModel: 'mailcontextviewmodel',

	/**
	 *
	 */
	constructor: function (config)
	{
		ERP.view.mail.MailContext.superclass.constructor.call(this, config);
		this.registerInsertionPoint('navigation.center', this.createContactNavigationPanel, this);
	},

	/**
	 * Override this method to return a new instance Ext.Panel.
	 * This instance will be placed at the center of the screen when the
	 * context is active.
	 * <p>
	 * The default implementation of getComponents() calls this method to
	 * lazily construct the toolbar.
	 * @return {Ext.Panel} a new panel instance
	 */
	createContentPanel : function()
	{
		return {
			xtype : 'erp.mailpanel',
			context : this
		};
	},
	/**
	 *
	 * @returns {{xtype: string, context: ERP.view.mail.MailContext, items: *[]}}
	 */
	createContactNavigationPanel : function()
	{
		return {
			xtype : 'contextnavigation',
			context : this,
			items : [{
				xtype : 'panel'
			}]
		};
	}
});

ERP.onReady(function(){
	container.registerContext(new ERP.view.core.ContextMetaData({
		name : 'mail',
		displayName: 'Mail',
		cls : 'erp erp-glasss',
		allowUserVisible : false,
		pluginConstructor : ERP.view.mail.MailContext
	}));
});