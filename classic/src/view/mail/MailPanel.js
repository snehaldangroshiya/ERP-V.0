Ext.namespace('ERP.view.mail');

Ext.define('ERP.view.mail.MailPanel', {
	/**
	 * 
	 */
	//extend : 'Ext.container.Container',
	extend : 'Ext.panel.Panel',
	/**
	 *
	 */
	alias : 'widget.erp.mailpanel',
	/**
	 *
	 */
	requires : [
		'ERP.view.core.ContextNavigationPanel'
	],

	/**
	 * @constructor
	 * @param config Configuration structure
	 */
	constructor : function(config)
	{
		config = config || {};
		Ext.create('Ext.data.Store', {
			storeId: 'simpsonsStore',
			fields:[ 'name', 'email', 'phone'],
			data: [
				{ name: 'Lisa', email: 'lisa@simpsons.com', phone: '555-111-1224' },
				{ name: 'Bart', email: 'bart@simpsons.com', phone: '555-222-1234' },
				{ name: 'Homer', email: 'homer@simpsons.com', phone: '555-222-1244' },
				{ name: 'Marge', email: 'marge@simpsons.com', phone: '555-222-1254' }
			]
		});
		Ext.applyIf(config, {
			xtype: 'mailpanel',
			title : 'Employees',
			layout  : 'fit',
			margin : '10',
			cls: 'shadow-panel',
			scrollable : true,
			items : [{
				xtype : 'grid',
				emptyText : 'No record in grid',
				deferEmptyText : true,
				store: Ext.data.StoreManager.lookup('simpsonsStore'),
				columns: [
					{ text: 'Name', dataIndex: 'name' },
					{ text: 'Email', dataIndex: 'email', flex: 1 },
					{ text: 'Phone', dataIndex: 'phone' }
				]
			}]
		});

		this.callParent(arguments);
	}
});
