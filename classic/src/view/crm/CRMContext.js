Ext.ns('ERP.view.crm');
/**
 *
 */
Ext.define('ERP.view.crm.CRMContext', {
	/**
	 *
	 */
	extend : 'ERP.view.core.Context',
	/**
	 *
	 */
	requires :[
		'ERP.view.crm.*'
	],

	/**
	 *
	 */
	constructor: function (config)
	{
		this.callParent(arguments);
	},

	/**
	 * Override this method to return a new instance Ext.Panel.
	 * This instance will be placed at the center of the screen when the
	 * context is active.
	 * <p>
	 * The default implementation of getComponents() calls this method to
	 * lazily construct the toolbar.
	 * @return {Ext.Panel} a new panel instance
	 */
	createContentPanel : function()
	{
		return {
			xtype : 'erp.crmpanel',
			context : this
		};
	}
});

ERP.onReady(function(){
	container.registerContext(new ERP.view.core.ContextMetaData({
		name : 'crm',
		displayName: 'CRM',
		cls : 'erp erp-glasss',
		allowUserVisible : false,
		pluginConstructor : ERP.view.crm.CRMContext
	}));
});