Ext.namespace('ERP.view.crm');

Ext.define('ERP.view.crm.CRMPanel', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.erp.crmpanel',
	/**
	 * @constructor
	 * @param config Configuration structure
	 */
	constructor : function(config)
	{
		config = config || {};

		Ext.applyIf(config, {
			xtype: 'crmpanel',
			layout  : 'fit',
			border : false
			//items : this.initHotelGrid(config.context)
		});
		this.callParent(arguments);
	}
});
